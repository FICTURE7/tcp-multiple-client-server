﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;


namespace TCP_MultiClient_Client
{
    public partial class main_client : Form
    {
        // client socket
        Socket server_socket;

        // check if server is connected or not
        bool server_connection = false;

        // create an ipendpoint for server address and port
        IPEndPoint server_address;

        public main_client()
        {
            InitializeComponent();
            // initializing client_socket
            server_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            server_address = new IPEndPoint(IPAddress.Parse("192.168.100.3"), 1000); // default values, can be override
        }

        private void main_client_Load(object sender, EventArgs e)
        {

        }

        private void connectBtn_Click(object sender, EventArgs e)
        {
            if (server_connection == false && connectBtn.Text == "Connect")
            {
                // update server information
                server_address.Address = IPAddress.Parse(ipaddressTxtBox.Text);
                server_address.Port = Convert.ToInt32(portTxtBox.Text);

                // establish connection to server
                server_socket.Connect(server_address);
                historyRichTxtBox.AppendText("Connected to Server");

                server_connection = true;
                connectBtn.Text = "Disconnect";
            }
            else if (server_connection == true  && connectBtn.Text == "Disconnect")
            {
                // close socket connection
                //server_socket.Shutdown(SocketShutdown.Both);
                server_socket.Disconnect(true);

                server_connection = false;
                connectBtn.Text = "Connect";
            }
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            messageTxtBox.Text = String.Empty;

            int msg_byte = server_socket.Send(Encoding.Default.GetBytes(messageTxtBox.Text));
            if (msg_byte > 0)
            {
                historyRichTxtBox.AppendText(System.String.Format("\n @Client: {0}", messageTxtBox.Text));
            }
        }
    }
}
