﻿namespace TCP_MultiClient_Server
{
    partial class main_server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.configserverGB = new System.Windows.Forms.GroupBox();
            this.lauchstartupChxBox = new System.Windows.Forms.CheckBox();
            this.connectBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.portTxtBox = new System.Windows.Forms.TextBox();
            this.ipaddressTxtBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.connectedclientListBox = new System.Windows.Forms.CheckedListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.sendBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.messageTxtBox = new System.Windows.Forms.TextBox();
            this.historyRichTxtBox = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.statusLable = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ClientCheckerTimer = new System.Windows.Forms.Timer(this.components);
            this.configserverGB.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // configserverGB
            // 
            this.configserverGB.Controls.Add(this.lauchstartupChxBox);
            this.configserverGB.Controls.Add(this.connectBtn);
            this.configserverGB.Controls.Add(this.label2);
            this.configserverGB.Controls.Add(this.label1);
            this.configserverGB.Controls.Add(this.portTxtBox);
            this.configserverGB.Controls.Add(this.ipaddressTxtBox);
            this.configserverGB.Location = new System.Drawing.Point(12, 12);
            this.configserverGB.Name = "configserverGB";
            this.configserverGB.Size = new System.Drawing.Size(842, 166);
            this.configserverGB.TabIndex = 0;
            this.configserverGB.TabStop = false;
            this.configserverGB.Text = "Configure Server";
            // 
            // lauchstartupChxBox
            // 
            this.lauchstartupChxBox.AutoSize = true;
            this.lauchstartupChxBox.Location = new System.Drawing.Point(33, 118);
            this.lauchstartupChxBox.Name = "lauchstartupChxBox";
            this.lauchstartupChxBox.Size = new System.Drawing.Size(269, 29);
            this.lauchstartupChxBox.TabIndex = 5;
            this.lauchstartupChxBox.Text = "Start Server On Launch";
            this.lauchstartupChxBox.UseVisualStyleBackColor = true;
            // 
            // connectBtn
            // 
            this.connectBtn.Location = new System.Drawing.Point(565, 67);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(218, 44);
            this.connectBtn.TabIndex = 4;
            this.connectBtn.Text = "Start Server";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(391, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Server Port:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(191, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Server IP Address:";
            // 
            // portTxtBox
            // 
            this.portTxtBox.Location = new System.Drawing.Point(396, 69);
            this.portTxtBox.Name = "portTxtBox";
            this.portTxtBox.Size = new System.Drawing.Size(149, 31);
            this.portTxtBox.TabIndex = 1;
            this.portTxtBox.Text = "13000";
            // 
            // ipaddressTxtBox
            // 
            this.ipaddressTxtBox.Location = new System.Drawing.Point(33, 69);
            this.ipaddressTxtBox.Name = "ipaddressTxtBox";
            this.ipaddressTxtBox.Size = new System.Drawing.Size(343, 31);
            this.ipaddressTxtBox.TabIndex = 0;
            this.ipaddressTxtBox.Text = "192.168.100.3";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.connectedclientListBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.sendBtn);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.messageTxtBox);
            this.groupBox1.Location = new System.Drawing.Point(23, 184);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(831, 521);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server Communication";
            // 
            // connectedclientListBox
            // 
            this.connectedclientListBox.FormattingEnabled = true;
            this.connectedclientListBox.Location = new System.Drawing.Point(22, 64);
            this.connectedclientListBox.Name = "connectedclientListBox";
            this.connectedclientListBox.Size = new System.Drawing.Size(737, 368);
            this.connectedclientListBox.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(183, 25);
            this.label7.TabIndex = 10;
            this.label7.Text = "Connected Client:";
            // 
            // sendBtn
            // 
            this.sendBtn.Location = new System.Drawing.Point(619, 465);
            this.sendBtn.Name = "sendBtn";
            this.sendBtn.Size = new System.Drawing.Size(140, 44);
            this.sendBtn.TabIndex = 6;
            this.sendBtn.Text = "Send";
            this.sendBtn.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 439);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(221, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "Message / Command:";
            // 
            // messageTxtBox
            // 
            this.messageTxtBox.Location = new System.Drawing.Point(20, 467);
            this.messageTxtBox.Name = "messageTxtBox";
            this.messageTxtBox.Size = new System.Drawing.Size(569, 31);
            this.messageTxtBox.TabIndex = 6;
            // 
            // historyRichTxtBox
            // 
            this.historyRichTxtBox.Location = new System.Drawing.Point(35, 69);
            this.historyRichTxtBox.Name = "historyRichTxtBox";
            this.historyRichTxtBox.Size = new System.Drawing.Size(705, 608);
            this.historyRichTxtBox.TabIndex = 8;
            this.historyRichTxtBox.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(261, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "Client Response / History:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 725);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "Server Status:";
            // 
            // statusLable
            // 
            this.statusLable.AutoSize = true;
            this.statusLable.Location = new System.Drawing.Point(183, 725);
            this.statusLable.Name = "statusLable";
            this.statusLable.Size = new System.Drawing.Size(74, 25);
            this.statusLable.TabIndex = 7;
            this.statusLable.Text = "Offline";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.historyRichTxtBox);
            this.groupBox2.Location = new System.Drawing.Point(881, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(760, 693);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Response / Log";
            // 
            // ClientCheckerTimer
            // 
            this.ClientCheckerTimer.Tick += new System.EventHandler(this.ClientCheckerTimer_Tick);
            // 
            // main_server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1708, 775);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.statusLable);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.configserverGB);
            this.Name = "main_server";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TCP Multiple Client Server";
            this.Load += new System.EventHandler(this.main_server_Load);
            this.configserverGB.ResumeLayout(false);
            this.configserverGB.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox configserverGB;
        private System.Windows.Forms.CheckBox lauchstartupChxBox;
        private System.Windows.Forms.Button connectBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox portTxtBox;
        private System.Windows.Forms.TextBox ipaddressTxtBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox historyRichTxtBox;
        private System.Windows.Forms.Button sendBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox messageTxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label statusLable;
        private System.Windows.Forms.CheckedListBox connectedclientListBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Timer ClientCheckerTimer;
    }
}

