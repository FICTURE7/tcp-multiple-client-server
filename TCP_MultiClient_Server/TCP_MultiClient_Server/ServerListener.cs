﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace TCP_MultiClient_Server
{
    class ServerListener
    {
        private Socket _ClientListener;

        public bool _IsListening { get; private set; }

        private string _ServerAddress { get; set; }

        private int _ServerPort { get; set; }

        public int _ConnectedClientCount { get; private set; }

        public Dictionary<string, Socket> _ClientLog = new Dictionary<string, Socket>();

        public void StartServer(string _address, int _port)
        {
            _ServerAddress = _address;
            _ServerPort = _port;

            IPEndPoint _ServerEndPoint = new IPEndPoint(IPAddress.Parse(_address), _port);

            _ClientListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            _ClientListener.Bind(_ServerEndPoint);
            _ClientListener.Listen(0);
            _IsListening = true;

            _ClientListener.BeginAccept(ClientListener_CallBack, _ClientListener);
        }

        private void ClientListener_CallBack(IAsyncResult async)
        {
            if (_IsListening == false)
                return;

            try
            {
                Socket _NewClientSocket = _ClientListener.EndAccept(async);
                _ConnectedClientCount = _ConnectedClientCount + 1;

                string _clientaddress = _NewClientSocket.RemoteEndPoint.ToString();

                _ClientLog.Add(String.Format("Client {0}",_ConnectedClientCount), _NewClientSocket);

                _ClientListener.BeginAccept(ClientListener_CallBack, _ClientListener);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void StopServer()
        {
            if (_IsListening == false)
                return;

            try
            {
                _ClientListener.Close();
                _ClientListener.Dispose();
                _IsListening = false;

                _ClientListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
    }
}