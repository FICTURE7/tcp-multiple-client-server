﻿using System;
using System.Windows.Forms;

namespace TCP_MultiClient_Server
{
    public partial class main_server : Form
    {
        ServerListener _ServerListener;
        int _CurrentConnectedUser;

        public main_server()
        {
            InitializeComponent();
            _ServerListener = new ServerListener();
            ClientCheckerTimer.Enabled = true;
        }

        private void main_server_Load(object sender, EventArgs e)
        {
            
        }

        private void connectBtn_Click(object sender, EventArgs e)
        {
            // run codes on Start Server button click
            string _inputaddress = String.Empty;
            int _inputport;

            // gather user input for address & port
            _inputaddress = ipaddressTxtBox.Text;
            _inputport = Convert.ToInt32(portTxtBox.Text);

            if (_ServerListener._IsListening == false)
            {
                _ServerListener.StartServer(_inputaddress, _inputport);
                connectBtn.Text = "Stop Server";

                historyRichTxtBox.AppendText("\n @Server: Starting Server, Please Wait...");
                if(_ServerListener._IsListening == true)
                {
                    historyRichTxtBox.AppendText("\n @Server: Server Started, Waiting for Clients");
                    statusLable.Text = System.String.Format("Server Listening at IP Address: {0} and on Port: {1}", _inputaddress, _inputport);
                    _CurrentConnectedUser = _ServerListener._ConnectedClientCount;

                }
            }
            else if (_ServerListener._IsListening == true)
            {
                _ServerListener.StopServer();
                connectBtn.Text = "Start Server";
                connectedclientListBox.DataSource = null;
            }
        }

        private void ClientCheckerTimer_Tick(object sender, EventArgs e)
        {
            if (_ServerListener._ConnectedClientCount > _CurrentConnectedUser)
            {
                _CurrentConnectedUser = _ServerListener._ConnectedClientCount;

                connectedclientListBox.DataSource = null;
                connectedclientListBox.DataSource = new BindingSource(_ServerListener._ClientLog, null);

                connectedclientListBox.DisplayMember = "Key";
                connectedclientListBox.ValueMember = "Value";
            }
        }
    }
}


